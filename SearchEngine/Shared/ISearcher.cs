﻿using System;
using System.Threading.Tasks;

namespace SearchEngine.Shared
{
    interface ISearcher
    {
        event Action<SearchResult> SearchCompleted;
        SearchResult Search(string phrase);
        Task SearchAsync(string phrase);
    }
}
