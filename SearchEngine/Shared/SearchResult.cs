﻿using Db;
using System.Collections.Generic;

namespace SearchEngine.Shared
{
    public class SearchResult
    {
        public string SearcherName { get; set; }
        public long ExecutionTime { get; set; }
        public IEnumerable<Articles> Articles { get; set; }
    }
}
