﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Db;
using SearchEngine.Shared;
using System.Threading.Tasks;

namespace SearchEngine.Sql
{
    public class SqlSearcher : ISearcher
    {
        public event Action<SearchResult> SearchCompleted;

        public SearchResult Search(string phrase)
        {
            var sw = new Stopwatch();
            var result = new SearchResult();

            sw.Start();
            var responseData = GetResponseData(phrase).ToList();
            sw.Stop();

            result.SearcherName = this.GetType().Name;
            result.Articles = responseData;
            result.ExecutionTime = sw.ElapsedMilliseconds;

            return result;
        }

        public Task SearchAsync(string phrase)
        {
            return Task.Run(() =>
            {
                var result = Search(phrase);
                SearchCompleted?.Invoke(result);
            });
        }

        private IEnumerable<Articles> GetResponseData(string phrase)
        {
            var searchCommand = "SELECT [Id], [Title], [Description] FROM Articles " +
                                "WHERE [Title] LIKE @phrase " +
                                "OR [Description] LIKE @phrase";

            using (var connection = new SqlConnection(DbContext.ConnectionString))
            {
                connection.Open();

                using (var cmd = new SqlCommand(searchCommand, connection))
                {
                    var searchPhrase = string.Concat('%', phrase, '%');

                    cmd.Parameters.Add(new SqlParameter("@phrase", searchPhrase));

                    using (var responseReader = cmd.ExecuteReader())
                    {
                        while (responseReader.Read())
                            yield return new Articles
                            {
                                Id = responseReader.GetGuid(0),
                                Title = responseReader.GetString(1),
                                Description = responseReader.GetString(2)
                            };
                    }
                }
            }
        }
    }
}
