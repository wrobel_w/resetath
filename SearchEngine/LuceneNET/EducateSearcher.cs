﻿using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using SearchEngine.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine.LuceneNET
{
    public class EducateSearcher
    {
        public void CreateIndex()
        {
            //Otwarcie katalogu indeksowania
            using (var directory = Lucene.Net.Store.FSDirectory.Open("/indexFolder/"))
            {
                //Utworzenie analizatora
                using (var analyzer = new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30))
                {
                    //Utworzenie IndexWriter'a
                    using (var writer = new Lucene.Net.Index.IndexWriter(directory, analyzer, true,
                        Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED))
                    {
                        //Utworzenie i wypełnienie dokumentu
                        var document = new Lucene.Net.Documents.Document();

                        var titleField = new Lucene.Net.Documents.Field(
                            "title",                                    //nazwa pola
                            "Koncert Anomalia",                         //wartość pola
                            Lucene.Net.Documents.Field.Store.YES,       //czy przechowywać wartość w indeksie
                            Lucene.Net.Documents.Field.Index.ANALYZED); //sposób indeksowania pola

                        document.Add(titleField);

                        writer.AddDocument(document);
                        writer.Optimize();
                    }

                }
            }
        }

        public void CreateQueryAndPerformSearch(string phrase)
        {
            using (var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30))
            {
                var query = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, "title", analyzer);
                var searchQuery = query.Parse(phrase);

                using (var directory = Lucene.Net.Store.FSDirectory.Open("/indexFolder/"))
                {
                    var collector = TopScoreDocCollector.Create(100, true);
                    var searcher = new IndexSearcher(directory, true);

                    searcher.Search(searchQuery, collector);

                    var hits = collector.TopDocs().ScoreDocs;

                    foreach (var hit in hits)
                    {
                        var title = searcher.Doc(hit.Doc).GetField("title");

                        Console.WriteLine("Score: {0} | Title: {1}", hit.Score, title);
                    }
                }
            }

        }
    }
}
