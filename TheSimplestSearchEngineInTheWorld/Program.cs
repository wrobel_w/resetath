﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimplestSearchEngineInTheWorld
{
    class Program
    {
        const string connectionString = @"data source=W-WROBEL2\SQLEXPRESS;initial catalog=FullText;integrated security=True";
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("\n\n\tWitaj w najprostszej wyszukiwarce na świecie!\n\n");
                Console.Write("\tWprowadź szukaną frazę: ");

                var phrase = Console.ReadLine();
                var searchedItems = Search(phrase).ToList();

                Console.WriteLine();

                foreach (var item in searchedItems)
                    Console.WriteLine("\t{0}", item.Title);

                Console.WriteLine("\n\tWyników: {0}\n", searchedItems.Count);
                Console.WriteLine("\tNaciśnij dowolny klawisz by wyszukać ponownie, ESC aby zakończyć");

                var key = Console.ReadKey();

                if (key.Key == ConsoleKey.Escape)
                    return;
                else
                    continue;
            }

        }

        private static IEnumerable<Article> Search(string phrase)
        {
            var searchCommand = "SELECT [Id], [Title] FROM Articles " +
                                "WHERE [Title] LIKE @phrase " +
                                "OR [Description] LIKE @phrase";

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (var cmd = new SqlCommand(searchCommand, connection))
                {
                    var searchPhrase = string.Concat('%', phrase, '%');

                    cmd.Parameters.Add(new SqlParameter("@phrase", searchPhrase));

                    using (var responseReader = cmd.ExecuteReader())
                    {
                        while (responseReader.Read())
                            yield return new Article
                            {
                                Id = responseReader.GetGuid(0),
                                Title = responseReader.GetString(1),
                            };
                    }
                }
            }
        }
    }
}
