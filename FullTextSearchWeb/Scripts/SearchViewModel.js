$(document).ready(function () { return ko.applyBindings(new SearchViewModel(), document.getElementById("search-container")); });
var SearchViewModel = (function () {
    function SearchViewModel() {
        var self = this;
        self.mainSearchInput = ko.observable();
        self.sqlSearchConfiguration = { type: eSearcher.Sql, phrase: ko.observable(), articles: ko.observableArray(), executionTime: ko.observable(0) };
        self.luceneSearchConfiguration = { type: eSearcher.Lucene, phrase: ko.observable(), articles: ko.observableArray(), executionTime: ko.observable(0) };
        self.examineSearchConfiguration = { type: eSearcher.Examine, phrase: ko.observable(), articles: ko.observableArray(), executionTime: ko.observable(0) };
        self.phraseChange = function (data, event) {
            if (data.phrase().length == 0) {
                data.phrase("");
                data.articles([]);
                data.executionTime(0);
            }
            else if (data.phrase().length > 3)
                $.ajax({
                    url: "/home/search",
                    data: {
                        phrase: data.phrase(),
                        searcherType: data.type
                    }
                }).then(function (result) {
                    data.articles(result.Articles);
                    data.executionTime(result.ExecutionTime);
                });
        };
        self.mainPhraseChange = function () {
            var update = function (conf) {
                conf.phrase(self.mainSearchInput());
                self.phraseChange(conf, null);
            };
            update(self.sqlSearchConfiguration);
            update(self.luceneSearchConfiguration);
        };
        self.openArticle = function (data) {
            var a = document.createElement("a");
            a.href = "/home/item/" + data.Id;
            a.target = "_blank";
            a.click();
        };
    }
    return SearchViewModel;
}());
var eSearcher;
(function (eSearcher) {
    eSearcher[eSearcher["Sql"] = 0] = "Sql";
    eSearcher[eSearcher["Lucene"] = 1] = "Lucene";
    eSearcher[eSearcher["Examine"] = 2] = "Examine";
})(eSearcher || (eSearcher = {}));
//# sourceMappingURL=SearchViewModel.js.map